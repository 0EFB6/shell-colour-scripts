# Shell Colour Scripts

A collection of terminal colour scripts.

# Installing shell-colour-scripts on Arch Linux

All you need to do is download the PKGBUILD from willy_package repository.  Then run the following command:

	makepkg -cf

This will create a file that ends in .pkg.tar.xz (for example, shell-colour-scripts-0.1-1-x86_64.pkg.tar.xz).  Then run:

	sudo pacman -U *.pkg.tar.zst

# Installing shell-colour-scripts on other Linux distrtibutions

Download the source code from this repository or use a git clone:

	git clone https://gitlab.com/willy45/shell-colour-scripts.git
	cd shell-colour-scripts
    rm -rf /opt/shell-colour-scripts || return 1
    sudo mkdir -p /opt/shell-colour-scripts/colourscripts || return 1
    sudo cp -rf colourscripts/* /opt/shell-colour-scripts/colourscripts
    sudo cp colourscript.sh /usr/bin/colourscript

    # optional for zsh completion
    sudo cp zsh_completion/_colourscript /usr/share/zsh/site-functions

# Usage

    colourscript --help
    Description: A collection of terminal colour scripts.

    Usage: colourscript [OPTION] [SCRIPT NAME/INDEX]
    -h, --help, help        	Print this help.
    -l, --list, list        	List all installed colour scripts.
    -r, --random, random    	Run a random colour script.
    -e, --exec, exec        	Run a specified colour script by SCRIPT NAME or INDEX.
    -a, --all, all          	List the outputs of all colourscripts with their SCRIPT NAME
    -b, --blacklist, blacklist	Blacklist a colour script by SCRIPT NAME or INDEX.

# The Scripts Are Located in /opt/shell-colour-scripts/colourscripts

The source for shell-colour-scripts is placed in:

	/opt/shell-colour-scripts/colourscripts

For even more fun, add the following line to your .bashrc or .zshrc and you will run a random colour script each time you open a terminal:

	colourscript random
