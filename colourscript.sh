#!/usr/bin/env bash

# Simple CLI for shell-colour-scripts

if [[ "$DEV" -gt 0 ]]; then
    DIR_COLOURSCRIPTS="./colourscripts"
else
    DIR_COLOURSCRIPTS="/opt/shell-colour-scripts/colourscripts"
fi

if command -v find &>/dev/null; then
    LS_CMD="$(command -v find) ${DIR_COLOURSCRIPTS} -maxdepth 1 -type f"
    LS_CMD_B="$(command -v find) ${DIR_COLOURSCRIPTS}/blacklisted -maxdepth 1 -type f"
else
    LS_CMD="$(command -v ls) ${DIR_COLOURSCRIPTS}"
    LS_CMD_B="$(command -v ls) ${DIR_COLOURSCRIPTS}/blacklisted"
fi

list_colourscripts="$($LS_CMD | xargs -I $ basename $ | cut -d ' ' -f 1 | nl)"
length_colourscripts="$($LS_CMD | wc -l)"
list_blacklist="$($LS_CMD_B 2>/dev/null | xargs -I $ basename $ | cut -d ' ' -f 1 | nl || "")" 
length_blacklist="$($LS_CMD_B 2>/dev/null | wc -l || 0)"

fmt_help="  %-20s\t%-54s\n"
function _help() {
    echo "Description: A collection of terminal colour scripts."
    echo ""
    echo "Usage: colourscript [OPTION] [SCRIPT NAME/INDEX]"
    printf "${fmt_help}" \
        "-h, --help, help" "Print this help." \
        "-l, --list, list" "List all installed colour scripts." \
        "-r, --random, random" "Run a random colour script." \
        "-e, --exec, exec" "Run a specified colour script by SCRIPT NAME or INDEX."\
        "-b, --blacklist, blacklist" "Blacklist a colour script by SCRIPT NAME or INDEX." \
        "-u, --unblacklist, unblacklist" "Unblacklist a colour script by SCRIPT NAME or INDEX." \
        "-a, --all, all" "List the outputs of all colourscripts with their SCRIPT NAME"
}

function _list() {
    echo "There are "$($LS_CMD | wc -l)" installed colour scripts:"
    echo "${list_colourscripts}"
}

function _list_blacklist() {
    echo "There are $length_blacklist blacklisted colour scripts:"
    echo "${list_blacklist}"
}

function _random() {
    declare -i random_index=$RANDOM%$length_colourscripts
    [[ $random_index -eq 0 ]] && random_index=1

    random_colourscript="$(echo  "${list_colourscripts}" | sed -n ${random_index}p \
        | tr -d ' ' | tr '\t' ' ' | cut -d ' ' -f 2)"
    # echo "${random_colourscript}"
    exec "${DIR_COLOURSCRIPTS}/${random_colourscript}"
}

function ifhascolourscipt() {
    [[ -e "${DIR_COLOURSCRIPTS}/$1" ]] && echo "Has this colour script."
}

function _run_by_name() {
    if [[ "$1" == "random" ]]; then
        _random
    elif [[ -n "$(ifhascolourscipt "$1")" ]]; then
        exec "${DIR_COLOURSCRIPTS}/$1"
    else
        echo "Input error, Don't have colour script named $1."
        exit 1
    fi
}

function _run_by_index() {
    if [[ "$1" -gt 0 && "$1" -le "${length_colourscripts}" ]]; then

        colourscript="$(echo  "${list_colourscripts}" | sed -n ${1}p \
            | tr -d ' ' | tr '\t' ' ' | cut -d ' ' -f 2)"
        exec "${DIR_COLOURSCRIPTS}/${colourscript}"
    else
        echo "Input error, Don't have colour script indexed $1."
        exit 1
    fi
}

function _run_colourscript() {
    if [[ "$1" =~ ^[0-9]+$ ]]; then
        _run_by_index "$1"
    else
        _run_by_name "$1"
    fi
}

function _run_all() {
    for s in $DIR_COLOURSCRIPTS/*
    do
        echo "$(echo $s | awk -F '/' '{print $NF}'):"
        echo "$($s)"
        echo
    done
}

function _blacklist_colourscript() { # by name only
    if [ ! -d "${DIR_COLOURSCRIPTS}/blacklisted" ]; then
        sudo mkdir "${DIR_COLOURSCRIPTS}/blacklisted"
    fi
    sudo mv "${DIR_COLOURSCRIPTS}/$1" "${DIR_COLOURSCRIPTS}/blacklisted"
}

function _unblacklist_colourscript() { # by name only
    if [ -f "${DIR_COLOURSCRIPTS}/blacklisted/$1" ]; then
        sudo mv "${DIR_COLOURSCRIPTS}/blacklisted/$1" "${DIR_COLOURSCRIPTS}"
    else
        echo "Input error. Script $1 is not blacklisted!"
    fi
}

case "$#" in
    0)
        _help
        ;;
    1)
        case "$1" in
            -h | --help | help)
                _help
                ;;
            -l | --list | list)
                _list
                ;;
            -b | --blacklist | blacklist)
                _list_blacklist
                ;;
            -r | --random | random)
                _random
                ;;
            -a | --all | all)
                _run_all
                ;;
            *)
                echo "Input error."
                exit 1
                ;;
        esac
        ;;
    2)
        if [[ "$1" == "-e" || "$1" == "--exec" || "$1" == "exec" ]]; then
            _run_colourscript "$2"
        elif [[ "$1" == "-b" || "$1" == "--blacklist" || "$1" == "blacklist" ]]; then
            _blacklist_colourscript "$2"
        elif [[ "$1" == "-u" || "$1" == "--unblacklist" || "$1" == "unblacklist" ]]; then
            _unblacklist_colourscript "$2"
        else
            echo "Input error."
            exit 1
        fi
        ;;
    *)
        echo "Input error, too many arguments."
        exit 1
        ;;
esac

